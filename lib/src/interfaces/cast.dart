class Cast {

  List<Actor> actors = new List();

  Cast.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    jsonList.forEach((item) {
      final actor = Actor.fromJsonMap(item);
      actors.add(actor);
    });
  }

}


class Actor {
  int castId;
  String character;
  String creditId;
  int gender;
  int id;
  String name;
  int order;
  String profilePath;

  Actor({
    this.castId,
    this.character,
    this.creditId,
    this.gender,
    this.id,
    this.name,
    this.order,
    this.profilePath,
  });


  Actor.fromJsonMap(Map<String, dynamic> json) {
    this.castId       = json['cast_id'];
    this.character    = json['character'];
    this.creditId     = json['credit_id'];
    this.gender       = json['gender'];
    this.id           = json['id'];
    this.name         = json['name'];
    this.order        = json['order'];
    this.profilePath  = json['profile_path'];
  }

  getPicture() {
    if (this.profilePath == null) {
      return 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQuRjuEz_pIx5qgi2jfrzKNzb-ePk0FDZ1mj3W862v3sC0fIrbm';
    } else {
      return 'https://image.tmdb.org/t/p/w500/$profilePath';
    }
  }
}

