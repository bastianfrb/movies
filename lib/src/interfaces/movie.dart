class Movies {
  List<Movie> items = new List();

  Movies();

  Movies.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for ( final item in jsonList) {
      final movie = new Movie.fromJsonMap(item);
      items.add(movie);
    }
  }
}

class Movie {
  List<Movie> results;
  int page;
  int totalResults;
  Dates dates;
  int totalPages;
  double popularity;
  int voteCount;
  bool video;
  String posterPath;
  int id;
  bool adult;
  String backdropPath;
  String originalLanguage;
  String originalTitle;
  List<int> genreIds;
  String title;
  double voteAverage;
  String overview;
  String releaseDate;
  String uniqueId;

  Movie({
    this.results,
    this.page,
    this.totalResults,
    this.dates,
    this.totalPages,
    this.popularity,
    this.voteCount,
    this.video,
    this.posterPath,
    this.id,
    this.adult,
    this.backdropPath,
    this.originalLanguage,
    this.originalTitle,
    this.genreIds,
    this.title,
    this.voteAverage,
    this.overview,
    this.releaseDate,
  });

  Movie.fromJsonMap(Map<String, dynamic> json) {
    results           = json['results'];
    page              = json['page'];
    totalResults      = json['total_results'];
    dates             = json['dates'];
    totalPages        = json['total_pages'];
    popularity        = json['popularity'] / 1;
    voteCount         = json['vote_count'];
    video             = json['video'];
    posterPath        = json['poster_path'];
    id                = json['id'];
    adult             = json['adult'];
    backdropPath      = json['backdrop_path'];
    originalLanguage  = json['original_language'];
    originalTitle     = json['original_title'];
    genreIds          = json['genre_ids'].cast<int>();
    title             = json['title'];
    voteAverage       = json['vote_average'] / 1;
    overview          = json['overview'];
    releaseDate       = json['release_date'];
  }

  String getPosterImage() {

    if (this.posterPath == null) {
      return 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQuRjuEz_pIx5qgi2jfrzKNzb-ePk0FDZ1mj3W862v3sC0fIrbm';
    } else {
      return 'https://image.tmdb.org/t/p/w500/$posterPath';
    }
  }

  String getBackdropImage() {

    if (this.backdropPath == null) {
      return 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQuRjuEz_pIx5qgi2jfrzKNzb-ePk0FDZ1mj3W862v3sC0fIrbm';
    } else {
      return 'https://image.tmdb.org/t/p/w500/$backdropPath';
    }
  }
}

class Dates {
  String maximum;
  String minimum;

  Dates({
    this.maximum,
    this.minimum,
  });
}
