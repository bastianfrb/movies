import 'package:flutter/material.dart';

class MovieSearch extends SearchDelegate {

  String selection = '';

  final movies = [
    'Batman',
    'Spiderman',
    'Aquaman',
    'Batman',
    'Shazam',
    'Hulk',
    'Thor',
    'Ironman',
    'Woody',
  ];

  final recentMovies = [
    'Spiderman',
    'Woody'
  ];

  @override
  List<Widget> buildActions(BuildContext context) {
    // Acciones dentro del input
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Ícono a la izquierda del AppBar
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // Resultados que vamos a mostrar
    return Center(
      child: Container(
        height: 100.0,
        width: 100.0,
        color: Colors.lightBlueAccent,
        child: Text(this.selection),
      )
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // Sugerencias que aparecen mientras la persona escribe

    final suggestedList = (query.isEmpty) ? 
                          recentMovies : 
                          movies.where((movie) => movie.toLowerCase().startsWith(query.toLowerCase())).toList();

    return ListView.builder(
      itemCount: suggestedList.length,
      itemBuilder: (context, i) {
        return ListTile(
          leading: Icon(Icons.movie),
          title: Text(suggestedList[i]),
          onTap: () { 
            this.selection = suggestedList[i];
            showResults(context);
          },
        );
      },
    );
  }

}