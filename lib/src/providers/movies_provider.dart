import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:movies/src/interfaces/movie.dart';
import 'package:movies/src/interfaces/cast.dart';


class MoviesProvider {

  String _apiKey    = 'db7e1b5a7d89f833ed538d5b49ade3ee';
  String _url       = 'api.themoviedb.org';
  String _language  = 'es-ES';
  int _popularPage = 0;
  List<Movie> _popular = new List();

  final _popularStreamController = StreamController<List<Movie>>.broadcast();

  Function(List<Movie>) get popularSink => _popularStreamController.sink.add;

  Stream<List<Movie>> get popularStream => _popularStreamController.stream;

  void disposeStream() {
    _popularStreamController?.close();
  }


  Future<List<Movie>> _processResponse(Uri url) async {
    final result = await http.get(url);
    final decodedData = json.decode(result.body);

    final movies = new Movies.fromJsonList(decodedData['results']);

    return movies.items;
  }

  Future<List<Movie>> getNowPlaying() async {
    final url = Uri.https(_url, '3/movie/now_playing', {
      'api_key' : this._apiKey,
      'language': this._language,
    });

    return await this._processResponse(url);
  }

  Future<List<Movie>> getPopular() async {

    this._popularPage ++;

    final url = Uri.https(_url, '3/movie/popular', {
      'api_key' : this._apiKey,
      'language': this._language,
      'page'    : this._popularPage.toString()
    });

    final response = await this._processResponse(url);

    this._popular.addAll(response);
    this.popularSink(this._popular);

    return response;
  }

  Future<List<Actor>> getCast(String movieId) async {
    final url = Uri.https(this._url, '3/movie/$movieId/credits', {
      'api_key' : this._apiKey,
      'language': this._language,
    });

    final response = await http.get(url);
    final decodedData = json.decode(response.body);

    final cast = new Cast.fromJsonList(decodedData['cast']);

    return cast.actors;
  }
}